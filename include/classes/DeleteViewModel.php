<?php

namespace GameApp;

class DeleteViewModel extends BaseViewModel {
	public function __construct() {
		parent::__construct();
		$this->deleteGame();
	}

	private function deleteGame() {
		$id = $_GET['id'] ?? 0;

		try {
			$this->pdo->query("DELETE FROM game WHERE id = $id");

			header('Location: index.php');
		} catch (\PdoException $e) {
			// log database error
		}
	}
}
