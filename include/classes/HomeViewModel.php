<?php
namespace GameApp;

class HomeViewModel extends BaseViewModel {
	public $games;

	public function __construct() {
		parent::__construct();
		$this->getGames();
	}

	private function getGames() {
		try {
			$sql = "SELECT g.id, g.title, g.release_year, s.short_name as `system_name`,
				d.name as `dev_name`
				FROM game g
				JOIN system s ON g.system_id = s.id
				JOIN developer d ON g.developer_id = d.id
				ORDER BY title";
			$this->games = $this->pdo->query($sql);
		} catch (\PdoException $e) {
			// log error
		}
	}
}
