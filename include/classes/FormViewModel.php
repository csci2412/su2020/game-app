<?php
namespace GameApp;

class FormViewModel extends BaseViewModel {
	private const USER_ID = 1;

	public $game;
	public $systems;
	public $developers;
	public $errors = [];

	public function __construct() {
		parent::__construct();
		$this->getGame();
		$this->getSystems();
		$this->getDevelopers();
		$this->handleSubmit();
	}

	/**
	 * we always want to check for an id query parameter
	 * to see if we need to get a game record to prepopulate
	 * the form
	 */
	private function getGame() {
		if (!isset($_GET['id'])) {
			return;
		}

		$id = $_GET['id'];

		try {
			$result = $this->pdo->query("SELECT * FROM game WHERE id = $id");

			if ($result->rowCount() === 1) {
				$this->game = $result->fetchObject();
			}
		} catch (\PdoException $e) {
			// log error
		}
	}

	/**
	 * we always need our list of systems to populate the
	 * dropdown
	 */
	private function getSystems() {
		try {
			$this->systems = $this->pdo->query('SELECT * FROM system ORDER BY short_name');
		} catch (\PdoException $e) {
			// log error
		}
	}

	/**
	 * we always need our list of developers to populate the
	 * dropdown
	 */
	private function getDevelopers() {
		try {
			$this->developers = $this->pdo->query('SELECT * FROM developer ORDER BY name');
		} catch (\PdoException $e) {
			// log error
		}
	}

	/**
	 * we always check if the form was submitted, and if so,
	 * insert or update a game record
	 */
	private function handleSubmit() {
		if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
			return;
		}

		try {
			// get data from $_POST array
			$id = $_POST['id'];
			$title = $_POST['title'];
			$year = $_POST['year'];
			$sysId = $_POST['system'];
			$devId = $_POST['developer'];
			$completed = isset($_POST['completed']) ? 1 : 0;

			// some simple input validation
			if (empty($title) || empty($year) || empty($sysId) || empty($devId)) {
				array_push($this->errors, 'Please enter title, year, system, and developer');
			}

			if (!is_numeric($year)) {
				array_push($this->errors, 'Please enter a valid year');
			}

			if (count($this->errors) > 0) {
				throw new \Exception('User input errors');
			}

			if (is_numeric($id)) {
				$sql = "UPDATE game set title='$title', release_year='$year', system_id=$sysId,
					developer_id=$devId WHERE id = $id";
			} else {
				$sql = "INSERT INTO game(title, release_year, system_id, developer_id)
					VALUES('$title', '$year', $sysId, $devId)";
			}
			// execute insert query
			$this->pdo->query($sql);

			// redirect to index.php
			header('Location: index.php');
		} catch(\PdoException $e) {
			// something happened with db query; log this
			print $e->getMessage();
			return;
		} catch (\Exception $e) {
			// log error
			return;
		}
	}
}
