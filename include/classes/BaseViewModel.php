<?php
namespace GameApp;

class BaseViewModel {
	// represents our database connection
	protected $pdo;

	public function __construct() {
		$this->pdo = self::getPdoConnection();
	}

	private static function getPdoConnection() {
		try {
			$pdoOptions = [ \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION ];
			return new \PDO('mysql:host=localhost;dbname=gamedb', 'xamppuser', 'welcome1', $pdoOptions);
		} catch (\PdoException $e) {
			// log error
			exit();
		}
	}
}
