<?php
require_once __DIR__ . '/../vendor/autoload.php';

use GameApp\BaseViewModel;
use GameApp\HomeViewModel;
use GameApp\FormViewModel;
use GameApp\DeleteViewModel;

// setup whoops error handling framework
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

// determine what page we're on
$file = $_SERVER['PHP_SELF'];

// $file is something like /su2020/unit8/index.php, all we care about is
// "index" part
preg_match('/\/([-a-z]+)\.php$/', $file, $matches);

if (count($matches) !== 2) {
	http_response_code(404);
	echo "Not Found";
	exit();
}

$route = $matches[1];

// instantiate the respective model we need
switch($route) {
	case 'index':
		$model = new HomeViewModel();
		break;
	case 'game-form':
		$model = new FormViewModel();
		break;
	case 'delete-game':
		$model = new DeleteViewModel();
		break;
	default:
		$model = new BaseViewModel();
		break;
}
