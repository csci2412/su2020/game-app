drop database if exists gamedb;
create database if not exists gamedb;
use gamedb;

create table `game`(
	`id` int unsigned auto_increment primary key,
    `title` nvarchar(255) not null,
    `release_year` year,
    `system_id` int unsigned not null,
    `developer_id` int unsigned # note this FK we are allowing to be null
);

create table `developer`(
	`id` int unsigned auto_increment,
    `name` nvarchar(255) not null,
    primary key(`id`)
);

create table `system`(
	`id` int unsigned auto_increment,
    `long_name` nvarchar(255) not null,
    `short_name` nvarchar(10),
    `release_year` year,
    primary key(`id`)
);

create table `user`(
	`id` int unsigned auto_increment,
    `username` nvarchar(255) not null,
    `password` char(64) not null,
    primary key(`id`)
);

create table `category`(
	`id` int unsigned auto_increment,
    `name` nvarchar(255) not null,
    primary key(`id`)
);

create table `game_user`(
	`game_id` int unsigned,
    `user_id` int unsigned,
    `completed` bool default false,
    `purchase_date` date,
    primary key(`game_id`, `user_id`)
);

create table `game_category`(
	`game_id` int unsigned,
    `category_id` int unsigned,
    primary key(`game_id`, `category_id`)
);

alter table `game` add constraint `developer_game_fk`
foreign key (`developer_id`) references `developer`(`id`)
on delete set null;

alter table `game` add constraint `system_game_fk`
foreign key (`system_id`) references `system`(`id`)
on delete cascade;

alter table `game_user` add constraint `game_game_user_fk`
foreign key (`game_id`) references `game`(`id`)
on delete cascade;

alter table `game_user` add constraint `user_game_user_fk`
foreign key (`user_id`) references `user`(`id`)
on delete cascade;

alter table `game_category` add constraint `game_game_category_fk`
foreign key (`game_id`) references `game`(`id`)
on delete cascade;

alter table `game_category` add constraint `category_game_category_fk`
foreign key (`category_id`) references `category`(`id`)
on delete cascade;

insert into `system`(`long_name`, `short_name`) values ('Nintendo', 'NES'),('Super Nintendo', 'SNES');
insert into `developer`(`name`) values ('Nintendo'),('Capcom'),('Konami');
insert into `category`(`name`) values ('Action'),('RPG'),('Strategy'),('Platformer');
insert into `user`(`username`,`password`) values ('andrew', sha2('welcome1', 256));

select * from `system`;
select * from `developer`;
select * from `category`;
select * from `user`;

insert into `game`(`title`, `release_year`, `system_id`, `developer_id`)
values ('Super Mario Bros', '1985', 1, 1);

insert into `game_user` values (1, 1, true, now());

select * from game_user;

insert into `game_category` values (1, 4);

select * from game_category;

select u.username, g.title, g.release_year, gu.completed, s.short_name 'system', d.name 'developer', c.name 'category'
from `user` u
join `game_user` gu on gu.user_id = u.id
join `game` g on g.id = gu.game_id
join `system` s on s.id = g.system_id
join `developer` d on d.id = g.developer_id
join `game_category` gc on gc.game_id = g.id
join `category` c on c.id = gc.category_id;

