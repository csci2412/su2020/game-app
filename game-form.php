<?php require_once 'include/header.php' ?>

<main>
	<nav>
		<a href="index.php">Home</a>
	</nav>
	<?php if (count($model->errors) > 0): ?>
		<ul class="errors">
			<?php foreach ($model->errors as $error): ?>
				<li><?=$error?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
	<form action="" method="post">
		<fieldset>
			<legend>Game Form</legend>
			<input type="hidden" name="id" value="<?=$model->game ? $model->game->id : ''?>">
			<div class="input-row">
				<label for="title">Title:</label>
				<input type="text" name="title" id="title"
					value="<?=$model->game ? $model->game->title : ''?>">
			</div>
			<div class="input-row">
				<label for="year">Release year:</label>
				<input type="text" name="year" id="year"
					value="<?=$model->game ? $model->game->release_year : ''?>">
			</div>
			<div class="input-row">
				<label for="system">System:</label>
				<select name="system" id="system">
					<option value=""></option>
					<?php foreach ($model->systems as $system): ?>
						<option
							value="<?=$system['id']?>"
							<?=$model->game && $model->game->system_id === $system['id'] ? 'selected' : ''?>
						>
							<?=$system['short_name']?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="input-row">
				<label for="developer">Developer:</label>
				<select name="developer" id="developer">
					<option value=""></option>
					<?php foreach ($model->developers as $developer): ?>
						<option
							value="<?=$developer['id']?>"
							<?=$model->game && $model->game->developer_id === $developer['id'] ? 'selected' : ''?>
						>
							<?=$developer['name']?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="input-row">
				<label for="completed">
					Completed:
					<input type="checkbox" name="completed" id="completed">
				</label>
			</div>
			<div class="input-row">
				<button type="submit">Save game</button>
			</div>
		</fieldset>
	</form>
</main>

<?php require_once 'include/footer.php' ?>
