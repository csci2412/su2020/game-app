<?php require_once 'include/header.php' ?>
<main>
<h1>Unit 8 - Game Collection Example</h1>
<nav>
	<a href="game-form.php">Create new game</a>
</nav>
<table>
	<th>Title</th>
	<th>Release Year</th>
	<th>System</th>
	<th>Developer</th>
	<th>Actions</th>
	<?php if ($model->games):?>
		<?php foreach ($model->games as $game): ?>
			<tr>
				<td><?=$game['title']?></td>
				<td><?=$game['release_year']?></td>
				<td><?=$game['system_name']?></td>
				<td><?=$game['dev_name']?></td>
				<td>
					<a href="game-form.php?id=<?=$game['id']?>">Edit</a>
					<a href="delete-game.php?id=<?=$game['id']?>"
						onclick="return confirm('Are you sure?')">Delete</a>
				</td>
			</tr>
		<?php endforeach; ?>
		<?php endif; ?>
</table>
</main>
<?php require_once 'include/footer.php' ?>
